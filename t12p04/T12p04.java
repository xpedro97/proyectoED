package t12p04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

public class T12p04
{
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int op;
        ConexionBD bd=new ConexionBD();
        
        /* ABRIR CONEXIÓN BD **************************************************/
        try {
            System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            System.out.println("Conexión abierta correctamente.");
        } catch (Exception e) {
            System.out.println("Error!!\n"+e.getMessage());
        }
        /**********************************************************************/
        
        do
        {
            System.out.println("");
            System.out.println("Seleccionar una opción:");
            System.out.println("-----------------------");
            System.out.println();
            System.out.println("1.- Alta de Curso.");
            System.out.println("2.- Baja de Curso.");
            System.out.println("3.- Alta de Alumno.");
            System.out.println("4.- Baja de Alumno.");
            System.out.println("5.- Listado de Cursos y Alumnos.");
            System.out.println("0.- Salir.");
            System.out.println();
            System.out.print("Opción? ");
            op=sc.nextInt();
            System.out.println("");

            switch (op)
            {
            case 1: // Alta de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.print("Introduce el título del curso: ");
                c.setTitulo(sc.next());
                System.out.print("Introduce las horas del curso: ");
                c.setHoras(sc.nextDouble());
                System.out.print("Introduce la fecha de inicio del curso (dd/mm/yyyy) (0:null): ");
                c.setFecIni(sc.next());
                System.out.print("Introduce la fecha de fin del curso (dd/mm/yyyy) (0:null): ");
                c.setFecFin(sc.next());
                System.out.print("Introduce la modalidad del curso (P)resencial o (T)elemático: ");
                c.setModalidad(sc.next().charAt(0));
                System.out.print("Introduce el estado del curso (P)rogramado, (R)ealizándose o (F)inalizado: ");
                char estado=sc.next().charAt(0);
                switch (estado) {
                    case 'P': c.setEstado(Curso.ESTADOCURSO.Programado); break;
                    case 'R': c.setEstado(Curso.ESTADOCURSO.Realizándose); break;
                    case 'F': c.setEstado(Curso.ESTADOCURSO.Finalizado); break;
                }
                System.out.println("");
                try {
                    c.altaCurso(bd);
                    System.out.println("Alta de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 2: // Baja de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.println("");
                try {
                    c.bajaCurso(bd);
                    System.out.println("Baja de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 3: // Alta de Alumno
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                try {
                    if (!c.existeCurso(bd)) {
                        System.out.println("El curso no existe!!");
                        break;
                    }
                    Alumno a=new Alumno();
                    a.setIdCurso(c.getId());
                    System.out.print("Introduce el dni del alumno: ");
                    a.setDni(sc.next());
                    System.out.print("Introduce el nombre del alumno: ");
                    a.setNombre(sc.next());
                    System.out.print("Es mayor de edad (s/n): ");
                    if (sc.next().equals("s")) a.setMayorEdad(true);
                    System.out.println("");
                    a.altaAlumno(bd);
                    System.out.println("Alta de alumno correcta. DNI: "+a.getDni()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 4: // Baja de Alumno
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                try {
                    if (!c.existeCurso(bd)) {
                        System.out.println("El curso no existe!!");
                        break;
                    }
                    Alumno a=new Alumno();
                    a.setIdCurso(c.getId());
                    System.out.print("Introduce el dni del alumno: ");
                    a.setDni(sc.next());
                    System.out.println("");
                    a.bajaAlumno(bd);
                    System.out.println("Baja de alumno correcta. DNI: "+a.getDni()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 5: // Listado de Cursos y Alumnos
            {
                System.out.println("LISTADO DE CURSOS Y ALUMNOS");
                System.out.println("---------------------------");
                List<Curso> tCursos=new ArrayList<>();
                List<Alumno> tAlumnos=new ArrayList<>();
                try {
                    Curso.listadoCursos(bd,tCursos);
                    Alumno.listadoAlumnos(bd,tAlumnos);
                    Collections.sort(tCursos);
                    Collections.sort(tAlumnos);
                    for (Curso c: tCursos) {
                        System.out.printf("ID: %-6d TÍTULO: %-10s HORAS: %6.2f FECINI: %10s FECFIN: %10s"
                                             + " MODALIDAD: %2s ESTADO: %12s\n",
                                            c.getId(),c.getTitulo(),c.getHoras(),
                                            c.getFecIni(),c.getFecFin(),
                                            c.getModalidad(),c.getEstado());
                        for (Alumno a: tAlumnos)
                        {
                            if (a.getIdCurso()==c.getId())
                                System.out.printf("\tDNI: %-10s NOMBRE: %-10s MAYORDEEDAD: %4s\n",
                                                    a.getDni(),a.getNombre(),a.isMayorEdad());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }                
                break;
            }   
            case 0: // Salir
            {
                /* CERRAR CONEXIÓN BD *****************************************/
                try {
                    System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    System.out.println("Conexión cerrada correctamente.");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                /**************************************************************/
                break;
            }
            default:
            {
                System.out.println("Opción incorrecta!!");
                System.out.println("");
            }
            }
        }
        while (op!=0);
        System.out.println("");
    
    }
    
}
